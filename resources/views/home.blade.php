@extends('layouts.app')

@section('content')
    <div class="row justify-content-center no-gutters">
        <div class="col-lg-10 headline">
            <h1>Hallo, <strong>{{ $user->name }}</strong></h1>
        </div>
    </div>

    <div class="row justify-content-center no-gutters">
        <div class="col-lg-10">
            @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> Es gab ein Problem mit dem Upload<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    <div class="row justify-content-center no-gutters">
        <div class="col-lg-10">
            <div class="card">
                <div class="card-header">Dateien hochladen</div>

                <div class="card-body">
                    <form class="upload-form" action="{{ route('upload') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="fileInput" aria-describedby="fileHelp">

                        <input type="submit" class="btn btn-secondary" value="hochladen">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center no-gutters">
        <div class="col-lg-10 headline">
            <h3>Uploads</h3>
        </div>
    </div>

    <div class="row justify-content-center no-gutters">
        <div class="col-lg-10">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Dateiname</th>
                        <th scope="col">Uploaddatum</th>
                        <th scope="col">Download</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($files as $file)

                    <tr>
                        <th scope="row"> {{ $file->id }}</th>
                        <td>{{ $file->name }}</td>
                        <td>{{ $file->created_at }}</td>
                        <td>
                            <a href="{{ secure_asset('storage/uploads/' . $file->location) }}" download> 
                                <span class="badge badge-secondary badge-pill">Download</span>
                            </a>
                        </td>  
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
