<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use DB;

class FileController extends Controller
{
	public function upload(Request $request) {
		$request->validate([
			'fileInput' => 'required'
		]);

		$user = Auth::user();

		// store new file
		$fileName = $user->id.'_file'.time().'.'.request()->fileInput->getClientOriginalExtension();
		$request->fileInput->storeAs('uploads', $fileName);

		// save upload in database
		DB::table('files')->insert([
			['name' => request()->fileInput->getClientOriginalName(), 'location' => $fileName]
		]);

		return back()->with('success', "Die Datei wurde erfolgreich hochgeladen");
	}
}